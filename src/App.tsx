import "./App.css";
import SideFilter from "./components/SideFilter/SideFilter";
import Users from "./components/Users/Users";

function App() {
  return (
    <div className="">
      {/* <Users /> */}
      <SideFilter />
    </div>
  );
}

export default App;
