import axios from "axios";
import { useState } from "react";

type getDataFuncType = (url: string) => void;

const useAxiosGets = <T>() => {
  const [resPonse, setResponse] = useState<T | []>();
  const [isLoading, setLoading] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string>("");

  const getData: getDataFuncType = (url) => {
    setLoading(true);
    axios
      .get(url)
      .then((res) => {
        if (res?.data) {
          setLoading(false);
          setResponse(res?.data);
          setErrorMessage("");
        }
      })
      .catch((err) => {
        setLoading(false);
        setResponse([]);
        setErrorMessage(err.message);
      });
  };
  return [resPonse, getData, isLoading, errorMessage, setResponse] as const;

};
export default useAxiosGets;
