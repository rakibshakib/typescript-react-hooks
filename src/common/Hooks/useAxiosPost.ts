import axios from "axios";
import { useState } from "react";

type postDataType = (
  url: string,
  payload?: [] | {},
  cb?: (res: any) => void,
  isToast?: boolean,
  successMessage?: string,
  errorMessage?: string
) => void;

type Methods = "post" | "put" | "patch" | "delete";

const useAxiosPost = <T>(method: Methods = "post") => {
  const [resData, setResData] = useState<T | []>();
  const [error, setError] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);
  const postData: postDataType  = (
    url,
    payload,
    cb,
    isToast = true,
    successMessage,
    errorMessage
  ) => {
    setLoading && setLoading(true);
    axios[method](url, payload)
      .then((res: any) => {
        setResData(res?.data);
        cb && cb(res?.data);
        setLoading(false);
      })
      .catch((err: any) => {
        setResData([]);
        setError(err);
        setLoading(false);
      });
  };
  return [resData, postData, loading, setResData, error] as const;
};

export default useAxiosPost;
