import { Checkbox } from "@mui/material";
import React, { useState } from "react";
import { filterMenu } from "./Utlis";
import FormControlLabel from "@mui/material/FormControlLabel";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";

type checkFunc = (
  value: boolean,
  subIndex: number,
  //   subItem: { value: number; label: string; isChecked?: boolean },
  item: any,
  index: number,
  key: string
) => void;

const FilerContainer = () => {
  const [filterList, setFiletList] = useState<ListStructue[]>(filterMenu);

  //   useEffect(() => {
  //     const modified = filterMenu?.map((item) => ({
  //       ...item,
  //       sublist: item?.subList?.map((subItem) => ({
  //         ...subItem,
  //         isChecked: false,
  //       })),
  //     }));
  //     setFiletList(modified);
  //   }, []);
  // console.log(filterList[2]);

  const [payload, setPayload] = useState<{}>({});
  const handleCheckBoxStatus: checkFunc = (
    value,
    subIndex,
    item,
    index,
    key
  ) => {
    const tempList = [...filterList];
    tempList[index].subList[subIndex].isChecked = value;

    // let payload: any[] = [];
    tempList.forEach((item) => {
      item.subList.forEach((items) => {
        // console.log(key, value);
        if (items?.label === key) {
          return setPayload({
            ...payload,
            [key]: value ? key : "N/A",
          });
        }
        // const obj ={
        //   [key]: value ? key : "N/A",
        // };
        // payload.push(obj)
      });
    });
    // console.log({ payload });
    // console.log({ tempList });
    setFiletList(tempList);
  };
  console.log({ payload });
  return (
    <div className="my-5">
      {filterList?.map((item, index) => {
        return (
          <div key={item?.titleId}>
            <h4 className="border py-1">
              {item?.title}
              <span>
                <ArrowDropDownIcon />
              </span>
            </h4>
            <div style={{ marginLeft: "15px" }}>
              {item?.subList?.map((subItem, subIndex) => {
                return (
                  <p key={subIndex} className="">
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={subItem?.isChecked}
                          onChange={(e) => {
                            const value = e?.target?.checked;
                            handleCheckBoxStatus(
                              value,
                              subIndex,
                              item,
                              index,
                              subItem?.label
                            );
                          }}
                          size="small"
                        />
                      }
                      label={subItem?.label}
                    />
                  </p>
                );
              })}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default FilerContainer;
