import React from "react";
import FilerContainer from "./FilerContainer";

const SideFilter = () => {
  return (
    <div className="container">
      {/* layout */}
      <div className="row border">
        <div className="col-md-4 border">
          filter layout
          <FilerContainer />
        </div>
        <div className="col-md-8 border">
          <p>Content</p>
        </div>
      </div>
    </div>
  );
};

export default SideFilter;
