
export const filterMenu: ListStructue[] = [
  {
    title: "Job Status",
    titleId: 1,
    subList: [
      { value: 1, label: "Open", isChecked: false },
      { value: 2, label: "Hire In Process", isChecked: false },
      { value: 3, label: "Draft",  isChecked:false },
      { value: 4, label: "Hire & Closed", isChecked:false },
      { value: 5, label: "Closed", isChecked:false },
    ],
  },
  {
    title: "Work Place Type",
    titleId: 2,
    subList: [
      { value: 1, isChecked: false, label: "On-Site" },
      { value: 2, isChecked: false, label: "Hybrid" },
      { value: 3, isChecked: false, label: "Remote" },
    ],
  },
  {
    title: "Employement Type",
    titleId: 3,
    subList: [
      { value: 1, isChecked: false, label: "Full Time" },
      { value: 2, isChecked: false, label: "Part Time" },
      { value: 3, isChecked: false, label: "Contract" },
      { value: 4, isChecked: false, label: "Temporary" },
      { value: 5, isChecked: false, label: "Volunteer" },
      { value: 6, isChecked: false, label: "Internship" },
    ],
  },
  {
    title: "Job Posted",
    titleId: 4,
    subList: [
      { value: 1, isChecked: false, label: "All" },
      { value: 2, isChecked: false, label: "Last Week" },
      { value: 3, isChecked: false, label: "Last 3 Month" },
      { value: 4, isChecked: false, label: "Last 6 Month" },
      { value: 5, isChecked: false, label: "This Year" },
    ],
  },
  {
    title: "Job Deadline",
    titleId: 5,
    subList: [
      { value: 1, isChecked: false, label: "Today" },
      { value: 2, isChecked: false, label: "Next Week" },
      { value: 3, isChecked: false, label: "Next 2 Week" },
      { value: 4, isChecked: false, label: "Next Month" },
      { value: 5, isChecked: false, label: "Next 2 Month" },
    ],
  },
];
