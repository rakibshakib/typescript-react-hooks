import React from "react";
import useAxiosGets from "../../common/Hooks/useAxiosGet";
import useAxiosPost from "../../common/Hooks/useAxiosPost";

interface userData {
  id?: number;
  name?: string;
  username?: string;
  email?: string;
  website?: boolean;
  company?: { name?: string; catchPhrase?: string; bs?: string } | {};
}

type getDataType = () => void;

const Users = () => {
  const [userList, getUsers, isLoadingUser, , setResponse] =
    useAxiosGets<userData[]>();
  const [, postData, lodingPost] = useAxiosPost();

  const getDataHandeler: getDataType = () => {
    getUsers(`https://jsonplaceholder.typicode.com/users`);
  };
  const getClearHandeler: getDataType = () => {
    setResponse([]);
  };
  const postMethodHandeler: getDataType = () => {
    postData(
      `https://jsonplaceholder.typicode.com/posts`,
      {
        title: "Rakibul",
        body: "Islam",
        userId: 1,
        data: "something",
      },
      (res) => {
        console.log(res);
      }
    );
  };

  console.log({ userList });
  console.log(typeof userList);

  return (
    <div>
      <div>
        <button
          onClick={() => {
            getDataHandeler();
          }}
        >
          Get Data
        </button>
        <button
          onClick={() => {
            getClearHandeler();
          }}
        >
          Clear Data
        </button>
        <button
          onClick={() => {
            postMethodHandeler();
          }}
        >
          Post Methdo
        </button>
      </div>
      {isLoadingUser && (
        <div>
          <p>Loading....</p>
        </div>
      )}
      <div>
        {userList?.map((item, idx) => (
          <div key={idx}>
            <p>{item.name}</p>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Users;
