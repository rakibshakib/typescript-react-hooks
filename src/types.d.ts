 interface SubList{
    value: number;
    label: string;
    isChecked: boolean;
}
 interface ListStructue {
  title: string;
  titleId: number;
  subList: SubList[];
}